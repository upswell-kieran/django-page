from django.conf.urls import patterns, url
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.db import models


class BasePageAdmin(admin.ModelAdmin):

    actions = ["make_published", "make_wfr", "make_wip", "set_template_name"]
    autocomplete_lookup_fields = {
        'fk': ('parent',)
    }
    date_hierarchy = ("created")
    list_display = ("title", "slug", "state", 'template_name',)
    list_display_links = ("title",)
    list_filter = ("state", "created_by__email")
    order = ("title",)
    prepopulated_fields = {"slug": ("title",)}
    raw_id_fields = ('parent',)
    readonly_fields = (
        "created_by", "created", "modified", "published",
    )
    search_fields = ("slug", "title", "content")

    def get_urls(self):
        urls = super(BasePageAdmin, self).get_urls()
        page_urls = patterns('',
            url(r'^(?P<page_id>\d+)/media_picker/$', self.media_picker,
                name='media_picker')
        )
        return page_urls + urls

    def media_picker(self, request, page_id):
        all_media = Media.objects.filter(id__gt=1)
        ctx = {
                "media_list": all_media,
                "opts": self.opts,
                "title": "media picker",
                }
        return render_to_response('admin/pages/media_picker.html',
                                  RequestContext(request, ctx))

    def make_published(self, request, queryset):
        for page in queryset:
            page.state = Page.PUBLISHED
            page.save()
    make_published.short_description = "Mark selected pages as published"

    def make_wfr(self, request, queryset):
        queryset.update(state=Page.WFR)
    make_wfr.short_description = "Mark selected pages as Waiting For Review"

    def make_wip(self, request, queryset):
        queryset.update(state=Page.WIP)
    make_wip.short_description = "Mark selected pages as WIP"

    def set_template_name(self, request, queryset):
        opts = self.model._meta
        app_label = opts.app_label
        if request.POST and request.POST.get("template_name"):
            queryset.update(template_name=request.POST.get("template_name"))
            return redirect('.')
        ctx = {
            'opts': opts,
            'app_label': app_label,
            'queryset': queryset
        }
        return render_to_response(
            template_name="admin/%s/set_template_name.html" % app_label,
            context_instance=RequestContext(request, ctx))
    set_template_name.short_description = "Set template name"

    def get_actions(self, request):
        actions = super(BasePageAdmin, self).get_actions(request)
        if not (request.user.is_superuser or
                request.user.has_perm('pages.can_publish')):
            if "make_published" in actions:
                del actions['make_published']
        if not request.user.is_superuser:
            del actions['set_template_name']
        return actions

    def save_model(self, request, obj, form, change):
        if not getattr(obj, "created_by"):
            obj.created_by = request.user
        super(BasePageAdmin, self). save_model(request, obj, form, change)
