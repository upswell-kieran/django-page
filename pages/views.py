from django.contrib.auth.views import login
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect, HttpResponseForbidden
from django.utils.translation import ugettext as _
from django.views.generic import DetailView

from .models import PageBase

class PageDetail(DetailView):
    
    model = None
    template_name = "templates/default.html"

    def get_queryset(self):
        queryset = self.model.objects.all()
        if not self.request.user.is_staff:
            queryset = queryset.published()
        return queryset

    def get_object(self, queryset=None):
        """
        Returns the Page for located at the `path` or 404
        """
        # We need to start by a /z
        path = "/" + self.kwargs.get('path', '')
        queryset = self.get_queryset()
        try:
            obj = queryset.get(path=path)
        except:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                    {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def render_to_response(self, context, **response_kwargs):
        #If page has special authentication:
        if self.object.authentication_required > 0:            
            if self.object.authentication_required >= Page.REGISTERED_USER and not self.request.user.is_authenticated():
                return HttpResponseRedirect( reverse("auth_login") )
            elif self.object.authentication_required >= Page.ADMIN and self.request.user.is_staff == False:
                return HttpResponseForbidden()
        
        #If page is a redirect page:
        if self.object.redirect_page == True:
            return HttpResponseRedirect( self.object.redirect_path )
            
        return super(PageDetail, self).render_to_response(context, **response_kwargs)

    def get_context_data(self, **kwargs):
        """
        Returns the context for the PageDetail template
        """
        ctx = super(PageDetail, self).get_context_data(**kwargs)
        ctx.update({
            "children":self.object.get_published_children(),
            "current":self.object.slug
        })
       
        return ctx

    def get_template_names(self):
        names = super(PageDetail, self).get_template_names()
        if self.object and hasattr(self.object, "template_name"):
            name = getattr(self.object, "template_name")
            if name:
                names.insert(0, name)
        return names
