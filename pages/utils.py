from os import path
from glob import glob
import os
import fnmatch

from django.conf import settings


def get_page_templates():
    """
    Build List of Available Page Templates

    Available Settings
    ------------------

    PAGES_DEFAULT_TEMPLATE - pages/default.html by default.
    PAGES_TEMPLATE_DIR - The base template directory for pages/
    PAGES_IGNORE_TEMPLATES - List of templates to ignore in the template list.
    """

    try:
        default_template = settings.PAGES_DEFAULT_TEMPLATE
    except AttributeError:
        default_template = "pages/default.html"

    try:
        base_template_dir = settings.PAGES_TEMPLATE_DIR
    except AttributeError:
        base_template_dir = 'pages/'

    try:
        ignore_templates = settings.PAGES_IGNORE_TEMPLATES
    except AttributeError:
        ignore_templates = (
            'pages/base.html'
        )

    def name(n):
        if n == default_template:
            return "Default"
        else:
            return n.replace(base_template_dir, '')\
                .replace('_', ' ')\
                .replace('.html', '')\
                .capitalize()

    files = []
    template_dir = settings.TEMPLATE_DIRS[0]
    for root, dirnames, filenames in os.walk(path.join(template_dir, 'pages')):
        for filename in fnmatch.filter(filenames, '*.html'):
            raw_file_name = os.path.join(root, filename)
            simple_path = raw_file_name.replace(template_dir+"/", "")
            files.append(str(simple_path))

    return [(i, name(i)) for i in files if not i in ignore_templates]
