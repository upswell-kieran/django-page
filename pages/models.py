from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

from .manager import PageManager
from .utils import get_page_templates


class PathMixin(models.Model):
    """
    Path Mixin
    ==========

    The Path Mixin provides the Pages app with the ability to nest pages and
    build a URL consisting of the parent and slug fields.
    """

    help = {
        'parent': 'The page this page will be nested under.',
        'path': "The URL path to this page, defined by the parent page path and the page text id.",
        'path_override': "Override to the default page url path defined in 'path' using the format: /my/custom/path",
        'redirect_page': "Redirect page instead of rendering it.",
        'redirect_path': "Path to redirect to.",
    }

    parent = models.ForeignKey('self', blank=True, null=True,
        related_name="children", help_text=help['parent'])

    path = models.CharField(_('path'), max_length=255, unique=True,
        help_text=help['path'])
    path_override = models.CharField(_('path override'), max_length=255,
        blank=True, help_text=help['path_override'])

    redirect_page = models.BooleanField(default=False, 
        help_text=help['redirect_page'])
    redirect_path = models.CharField(_('Redirect Path'), max_length=255,
        blank=True, help_text=help['redirect_path'])

    class Meta:
        abstract = True

    @staticmethod
    def autocomplete_search_fields():
        return ("title__icontains",)

    def _build_path(self):
        if self.path_override:
            return self.path_override
        elif self.parent:
            return "%s%s/" % (self.parent.path, self.slug)
        else:
            return "/%s/" % self.slug

    def get_pagetree(self):
        def _get_parent_obj(page, tree):
            if page.parent:
                tree.append(page.parent)
                _get_parent_obj(page.parent, tree)
        tree = [self]
        _get_parent_obj(self, tree)
        tree.reverse()
        return tree

    def update_path(self):
        previous_path = self.path if self.pk else None
        self.path = self._build_path()
        
    def update_children(self):
        [p.save() for p in self.get_children()]


class SEOMixin(models.Model):
    """
    SEO Mixin
    =========

    Fields to support old HTML Meta SEO fields description and keywords.
    """
    help = {
        'page_meta_description': "A short description of the page, used for SEO and not displayed to the user.",
        'page_meta_keywords': "A short list of keywords of the page, used for SEO and not displayed to the user.",
    }

    page_meta_description = models.CharField(_('Meta Description'), 
        max_length=2000, blank=True, help_text=help['page_meta_description'])
    page_meta_keywords = models.CharField(_('Meta Page Keywords'), 
        max_length=2000, blank=True, help_text=help['page_meta_keywords'])

    class Meta:
        abstract = True


class PageBase(models.Model):

    # -- Choice Data
    WIP = 10
    WFR = 15
    PUBLISHED = 20
    STATE_CHOICES = (
        (WIP, "Work in Progress"),
        (WFR, "Waiting for Review"),
        (PUBLISHED, "Published")
    )

    NONE = 0
    REGISTERED_USER = 10
    ADMIN = 100
    AUTHENTICATION_CHOICES = (
        (NONE, "None"),
        (REGISTERED_USER, "Registered User"),
        (ADMIN, "Admin")
    )

    # -- Help Text Strings
    help = {
        'title': "The display title for this page. No more than 5 words, 28 characters with spaces.",
        'slug': "Auto-generated page text id for this page.",
        'sub_title': "A brief description of the page. About a sentance long."
    }

    slug = models.CharField(_('Text ID'), max_length=255, blank=True, 
        unique=True, db_index=True, help_text=help['slug'])

    title = models.CharField(_('Page Title'), max_length=255, 
        help_text=help['title'])
    sub_title = models.CharField(_('Sub Title'), max_length=2000, blank=True,
        help_text=help['sub_title'])

    # -- Page Content
    template_name = models.CharField(_('Template Name'), max_length=255, 
        default="pages/default.html", choices=get_page_templates())
    content = models.TextField(_('Page Content'), blank=True)

    # -- State    
    state = models.IntegerField(choices=STATE_CHOICES, default=WIP)
    authentication_required = models.IntegerField(choices=AUTHENTICATION_CHOICES, 
        default=NONE)
    order = models.PositiveSmallIntegerField(default=1)

    # -- Meta Data
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, 
        blank=True, null=True, related_name='%(class)s_created_by')

    created = models.DateTimeField(_('Created Date'), auto_now_add=True, 
        blank=True, null=True)
    modified = models.DateTimeField(_('Modified Date'), auto_now=True, 
        blank=True, null=True)
    published = models.DateTimeField(_('Published Date'), blank=True, 
        null=True)

    objects = PageManager()

    class Meta:
        abstract = True
        verbose_name = "Page"
        verbose_name_plural = "Pages"
        ordering = ('path',)
        permissions = (
            ("can_publish", "Can Publish"),
        )

    def __unicode__(self):
        return "%s [%s]" % (self.title, self.path)

    def get_absolute_url(self):
        return reverse("pages_page", args=[], kwargs={"path": self.path[1:]})
    
    def get_children(self):
        return self.__class__.objects.filter(parent=self)

    def get_published_children(self):
        return self.__class__.objects.published().filter(parent=self)

    def save(self, *args, **kwargs):
        if self.pk:
            original = self.__class__.objects.get(pk=self.pk)
        else:
            original = None
        
        # -- Update published date
        if (original!=None) and (original.state != self.PUBLISHED) and (self.state == self.PUBLISHED):
            self.published = now()

        super(PageBase, self).save(*args, **kwargs)
