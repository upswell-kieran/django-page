#!/usr/bin/env python
from setuptools import setup, find_packages
setup(name = 'django_pages',
    description = 'A basic page model with templating, versioning, and basic authorization',
    version = '1.1',
    url = 'https://bitbucket.org/cgpartners/django-page',
    author="C&G Partners, LLC",
    author_email='kieran@cgpartnersllc.com',
    packages=find_packages(exclude=['ez_setup']),
    zip_safe = False,
    include_package_data=True,
    install_requires = [
        'setuptools', 
        'Django',
        'django-reversion', 
        'django_ckeditorfiles', 
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved',
        'Operating System :: OS Independent',
        'Programming Language :: Python'
    ]
)
