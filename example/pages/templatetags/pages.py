from django.template import Library

from ..models import Page

register = Library()


@register.assignment_tag(takes_context=True)
def get_page(context, slug):
    try:
        return (Page.objects.filter(slug=slug)
                            .defer("path", "title", "slug", "state")
                            .get())
    except Page.DoesNotExist:
        return ""


@register.assignment_tag()
def get_subpages(slug, limit=None):
    try:
        p = Page.objects.get(slug=slug)
        pages = p.children.all().order_by('order')
        if limit:
            return pages[:limit]
        return pages
    except Page.DoesNotExist:
        return []
