from django.conf.urls import patterns, url, include

from .views import PageDetail

urlpatterns = patterns('',
    url(r'^(?P<path>[-_\/\w]*)$', PageDetail.as_view(), name="pages_page"),
)
