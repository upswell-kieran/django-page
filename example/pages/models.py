from django.db import models

from pages.models import PageBase, PathMixin, SEOMixin


class Page(PageBase, PathMixin, SEOMixin):

    related_pages = models.ManyToManyField('self', blank=True, symmetrical=False)

    def save(self, *args, **kwargs):
        self.update_path()
        super(Page, self).save(*args, **kwargs)
        self.update_children() 
