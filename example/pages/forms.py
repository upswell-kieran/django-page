from django import forms
from pages.forms import PageContentWidget

from .models import Page

class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        widgets = {
            'content': PageContentWidget()
        }
