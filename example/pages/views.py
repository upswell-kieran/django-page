from pages.views import PageDetail as AbstractPageDetail

from .models import Page

class PageDetail(AbstractPageDetail):
    model = Page
