from django.contrib import admin

from pages.admin import BasePageAdmin

from .forms import PageForm
from .models import Page


class ChildPageInlines(admin.TabularInline):
    model = Page
    fk_name = "parent"
    sortable_field_name = "order"
    extra = 0
    max_num = 0
    fieldsets = (
        ("", {
            'fields': (
                ('title', 'slug', 'path', 'order',),
            )
        }),
    )
    readonly_fields = ('title', 'path')


class PageAdmin(BasePageAdmin):

    filter_horizontal = ["related_pages"]
    form = PageForm
    inlines = [ChildPageInlines]
    list_display = (
        "title", "path", "redirect_path", "parent", "slug", "state", 
        'template_name',
    )
    readonly_fields = (
        "created_by", "created", "modified", "published","path"
    )
    search_fields = ("slug", "title", "content")
    
    fieldsets = (
        ("Main Body", {
            'fields': (
                'parent',
                'title',
                'sub_title',
                'template_name',
                'content',
            )
        }),
        ("State", {
            'fields': (
                ('state', 'authentication_required'),           
            )
        }),
        ("Page Path", {
            'fields': (
                'slug',
                ('path', 'path_override'),
                ('redirect_page', 'redirect_path'),
            ),
            'classes': ( 'grp-collapse grp-closed',)
        }),
        ("SEO", {
            'fields': (
                'page_meta_description',
                'page_meta_keywords'
            )
        }),
        ("Content Meta", {
            'fields': (
                'related_pages',
            ),
            'classes': ( 'grp-collapse grp-closed',)
        }),
         ("Meta", {
            'fields': (
                ('created_by', 'created'),
                ('modified', 'published'),
            ),
            'classes': ( 'grp-collapse grp-closed', )
        }),
    )



admin.site.register(Page, PageAdmin)
