## Installation


Installtion is currently only supported by pip via git. Get started:

```bash
pip install git+ssh://git@bitbucket.org/cgpartners/django-page.git#egg=django_pages
python manage.py syncdb --migrate
```


## Configuration

Pages was designed to be abstract by default, while this means that it has to be
implemented within a separate app in your project, it also means that the models
and functionality are extensible.

At minimum you will need to implementent:

* admin.py
* forms.py
* models.py
* urls.py
* views.py

For simplicity sake, the below implementation has been added to the
`example` folder of this package.


### admin.py


```python
from django.contrib import admin

from pages.admin import BasePageAdmin

from .forms import PageForm
from .models import Page

class PageAdmin(BasePageAdmin):
    pass

admin.site.register(Page, PageAdmin)
```

### forms.py

```python
from django import forms
from pages.forms import PageContentWidget

from .models import Page

class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        widgets = {
            'content': PageContentWidget()
        }
```


### models.py

```python
from django.db import models

from pages.models import PageBase, PathMixin, SEOMixin

class Page(PageBase, PathMixin, SEOMixin):
    def save(self, *args, **kwargs):
        self.update_path()
        super(Page, self).save(*args, **kwargs)
        self.update_children() 
```


### urls.py

```python
from django.conf.urls import patterns, url, include

from .views import PageDetail

urlpatterns = patterns('',
    url(r'^(?P<path>[-_\/\w]*)$', PageDetail.as_view(), name="pages_page"),
)
```

### views.py

```python
from pages.views import PageDetail as AbstractPageDetail

from .models import Page

class PageDetail(AbstractPageDetail):
    model = Page
```
